CC = gcc

all:
	clang++ -g -std=c++11 main.cpp

concurrent_queue.o: concurrent_queue.c
	$(CC) -g -c concurrent_queue.c

ThreadPool.o: ThreadPool.c
	$(CC) -g -c ThreadPool.c
