#include "ThreadPool.h"

int ThreadPool_init(ThreadPool *thread_pool, usize count,
                    uword (*thread_routine)(uword))
{
    int status;

    status = pthread_mutex_init(&thread_pool->mutex, NULL);
    if (status < 0)
        goto pthread_mutex_init_error;

    thread_pool->threads = malloc(sizeof(pthread_t) * count);
    if (!thread_pool->threads)
        goto malloc_error;

    thread_pool->count = count;
    thread_pool->active = count;

    for (usize i = 0; i < count; ++i)
    {
        status = pthread_create(&thread_pool->threads[i], NULL,
                                thread_routine, NULL);
        if (status < 0) {
            fprintf(stderr, "Error: %s: pthread_create: %d\n", __FUNCTION__, status);
            pthread_mutex_lock(&thread_pool->mutex);
                thread_pool->active -= 1;
            pthread_mutex_unlock(&thread_pool->mutex);
        }
    }

    return 0;

pthread_mutex_init_error:
    fprintf(stderr, "Error: %s: pthread_mutex_init: %d\n", __FUNCTION__, status);
    goto failure;
malloc_error:
    fprintf(stderr, "Error: %s: malloc: %d\n", __FUNCTION__, status);
    pthread_mutex_destroy(&thread_pool->mutex);
    goto failure;
failure:
    return -1;
}

int ThreadPool_join(ThreadPool *thread_pool)
{
    int status;
    int retval = 0;
    for (usize i = 0; i < thread_pool->count; ++i) {
        status = pthread_join(thread_pool->threads[i], NULL);
        if (status < 0) {
            fprintf(stderr, "Error: ThreadPool_join: pthread_join: %d\n", status);
            retval = -1;
        }
    }
    return retval;
}

void ThreadPool_free(ThreadPool *thread_pool)
{
    free(thread_pool->threads);
    pthread_mutex_destroy(&thread_pool->mutex);
}
