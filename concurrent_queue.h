#ifndef concurrent_queue_h
#define concurrent_queue_h
#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>

typedef size_t  usize;
typedef void *  uword;
typedef void    (*free_function)(uword);

struct concurrent_queue
{
    usize head;
    usize tail;
    usize size;
    uword *data;
    sem_t *empty;
    sem_t *full;
    sem_t *mutex;
};

typedef struct concurrent_queue concurrent_queue;

int concurrent_queue_init(concurrent_queue *cq, usize size);
void concurrent_queue_free(concurrent_queue *cq, free_function f);
int concurrent_queue_enqueue(concurrent_queue *cq, uword word);
int concurrent_queue_dequeue(concurrent_queue *cq, uword *ptr);
int concurrent_queue_try_dequeue(concurrent_queue *cq, uword *ptr);
int concurrent_queue_print_debug(concurrent_queue *cq);
usize concurrent_queue_size(concurrent_queue *cq);

#endif
