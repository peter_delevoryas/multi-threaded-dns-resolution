#include "concurrent_queue.h"

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <semaphore.h>
#include <unistd.h>

static char *semaphore_unique_cstr()
{
    static int sid = 0;
    static char buf[13];
    pid_t pid = getpid();

    sprintf(buf, "sem%05d%03d", pid, sid);
    sid += 1;
    return buf;
}

int concurrent_queue_init(concurrent_queue *cq, usize size)
{
    assert(cq);

    cq->data = malloc(sizeof(uword) * size);
    if (!cq->data)
    {
        fprintf(stderr, "Error: %s: malloc\n", __FUNCTION__);
        goto malloc_error;
    }

    for (usize i = 0; i < size; ++i)
    {
        cq->data[i] = NULL;
    }

    cq->empty = sem_open(semaphore_unique_cstr(), O_CREAT, 644, size);
    if (!cq->empty)
    {
        goto sem_open_empty_error;
    }
    cq->full = sem_open(semaphore_unique_cstr(), O_CREAT, 644, 0);
    if (!cq->full)
    {
        goto sem_open_full_error;
    }
    cq->mutex = sem_open(semaphore_unique_cstr(), O_CREAT, 644, 1);
    if (!cq->mutex)
    {
        goto sem_open_mutex_error;
    }

    cq->head = 0;
    cq->tail = 0;
    cq->size = size;

    return 0;

sem_open_mutex_error:
    sem_close(cq->full);
sem_open_full_error:
    sem_close(cq->empty);
sem_open_empty_error:
    free(cq->data);
    cq->data = NULL;
malloc_error:
    return -1;
}

void concurrent_queue_free(concurrent_queue *cq, free_function f)
{
    if (f)
    {
        for (usize i = 0; i < cq->size; ++i)
        {
            if (cq->data[i])
            {
                f(cq->data[i]);
            }
        }
    }

    if (cq->data)
    {
        free(cq->data);
    }
}

int concurrent_queue_enqueue(concurrent_queue *cq, uword word)
{
    int status;

    // Enter critical section/block until available
    status = sem_wait(cq->empty);
    if (status < 0)
    {
        goto sem_wait_error;
    }
    status = sem_wait(cq->mutex);
    if (status < 0)
    {
        goto sem_wait_error;
    }
    
    // Aquired exclusive access to valid tail
    cq->data[cq->tail] = word;
    cq->tail = (cq->tail + 1) % cq->size;

    // Unlock semaphores
    status = sem_post(cq->mutex);
    if (status < 0)
    {
        goto sem_post_error;
    }
    status = sem_post(cq->full);
    if (status < 0)
    {
        goto sem_post_error;
    }

    return 0;

sem_wait_error:
    fprintf(stderr, "Error: %s: sem_wait: %d\n", __FUNCTION__, status);
    return -1;

sem_post_error:
    fprintf(stderr, "Error: %s: sem_post: %d\n", __FUNCTION__, status);
    return -1;
}

int concurrent_queue_dequeue(concurrent_queue *cq, uword *ptr)
{
    int status;

    status = sem_wait(cq->full);
    if (status < 0)
    {
        goto sem_wait_error;
    }
    status = sem_wait(cq->mutex);
    if (status < 0)
    {
        goto sem_wait_error;
    }

    *ptr = cq->data[cq->head];
    cq->head = (cq->head + 1) % cq->size;

    status = sem_post(cq->mutex);
    if (status < 0)
    {
        goto sem_post_error;
    }
    status = sem_post(cq->empty);
    if (status < 0)
    {
        goto sem_post_error;
    }

    return 0;

sem_wait_error:
    fprintf(stderr, "Error: %s: sem_wait: %d\n", __FUNCTION__, status);
    return -1;

sem_post_error:
    fprintf(stderr, "Error: %s: sem_post: %d\n", __FUNCTION__, status);
    return -1;
}

int concurrent_queue_try_dequeue(concurrent_queue *cq, uword *ptr)
{
    int status;

    status = sem_trywait(cq->full);
    // Failed to acquire lock immediately
    if (status < 0) {
        return -1;
    }

    status = sem_wait(cq->mutex);
    if (status < 0) {
        perror("sem_wait(concurrent_queue.mutex)");
        return -1;
    }

    *ptr = cq->data[cq->head];
    cq->head = (cq->head + 1) % cq->size;

    status = sem_post(cq->mutex);
    if (status < 0) {
        perror("sem_post(concurrent_queue.mutex)");
        // Don't return, attempt to signal cq->empty
    }

    status = sem_post(cq->empty);
    if (status < 0) {
        perror("sem_post(concurrent_queue.empty");
        return -1;
    }

    return 0;
}

int concurrent_queue_print_debug(concurrent_queue *cq)
{
    int status;

    status = sem_wait(cq->mutex);
    if (status < 0)
    {
        goto sem_wait_error;
    }
    fprintf(stderr, "concurrent_queue:\n");
    for (usize i = cq->head; i < cq->size; ++i)
    {
        usize data_offset = i % cq->size;
        fprintf(stderr, "%3zu: %20p\n", i, (void*)cq->data[data_offset]);
    }
    status = sem_post(cq->mutex);
    if (status < 0)
    {
        goto sem_post_error;
    }

    return 0;

sem_wait_error:
    fprintf(stderr, "Error: %s: sem_wait: %d\n", __FUNCTION__, status);
    return -1;

sem_post_error:
    fprintf(stderr, "Error: %s: sem_post: %d\n", __FUNCTION__, status);
    return -1;
}

usize concurrent_queue_size(concurrent_queue *cq)
{
    usize size = 0;
    int status;

    status = sem_wait(cq->mutex);
    if (status < 0)
        goto sem_wait_error;
    size = cq->size;
    status = sem_post(cq->mutex);
    if (status < 0)
        goto sem_post_error;

    return size;

sem_wait_error:
    fprintf(stderr, "Error: %s: sem_wait: %d\n", __FUNCTION__, status);
    return size;
sem_post_error:
    fprintf(stderr, "Error: %s: sem_post: %d\n", __FUNCTION__, status);
    return size;
}


