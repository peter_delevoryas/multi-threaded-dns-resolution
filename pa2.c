#include "concurrent_queue.h"
#include "ThreadPool.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <err.h>
#define MIN_ARGS 4
#define URL_QUEUE_SIZE 5

typedef size_t  usize;
typedef void *  uword;

// GLOBAL VARIABLES
pthread_mutex_t     _stdout_mutex;
pthread_mutex_t     _input_files_remaining_mutex;
pthread_mutex_t     _getaddrinfo_mutex;
usize               _input_files_remaining;
concurrent_queue    _input_file_queue;
concurrent_queue    _url_queue;
ThreadPool          _request_handlers;
ThreadPool          _url_handlers;

uword request_handler(uword thread_arg);
uword url_handler(uword thread_arg);

int main(int argc, const char *argv[])
{
    const char *output_filename;
    const char **input_filenames;
    usize n_input_filenames;
    usize n_threads;
    usize n_request_handlers;
    usize n_url_handlers;
    FILE *file;
    int status;

    if (argc < MIN_ARGS) {
        fprintf(stderr, "usage: %s [input ...] [output] [n_threads]\n", argv[0]);
        return -1;
    }
    else {
        output_filename = argv[argc - 2];
        n_input_filenames = argc - 3;
        input_filenames = argv + 1;
        n_threads = strtoul(argv[argc - 1], NULL, 10);
        n_request_handlers = (n_threads % 2) == 0 ? n_threads / 2 : n_threads / 2 + 1;
        n_url_handlers = (n_threads % 2) == 0 ? n_threads / 2 : n_threads / 2;
    }

    // initialize FILE * queue
    status = concurrent_queue_init(&_input_file_queue, n_input_filenames);
    if (status < 0) {
        fprintf(stderr, "Error initializing input file queue\n");
        return -1;
    }

    // create work queue
    for (usize i = 0; i < n_input_filenames; ++i) {
        file = fopen(input_filenames[i], "r");
        if (file) {
            concurrent_queue_enqueue(&_input_file_queue, (uword)file);
        }
        else {
            fprintf(stderr, "Failed to open %s\n", input_filenames[i]);
        }
    }

    // initialize URL queue
    status = concurrent_queue_init(&_url_queue, URL_QUEUE_SIZE);
    if (status < 0) {
        fprintf(stderr, "Error initializing URL queue\n");
        return -1;
    }

    // initialize _input_files_remaining
    pthread_mutex_init(&_input_files_remaining_mutex, NULL);
    _input_files_remaining = _input_file_queue.size;

    pthread_mutex_init(&_stdout_mutex, NULL);
    pthread_mutex_init(&_getaddrinfo_mutex, NULL);

    // spin up the thread pool
    ThreadPool_init(&_request_handlers, n_request_handlers, request_handler);
    ThreadPool_init(&_url_handlers, n_url_handlers, url_handler);

    // Finish all work
    ThreadPool_join(&_request_handlers);
    ThreadPool_join(&_url_handlers);

    // free concurrent queue
    concurrent_queue_free(&_input_file_queue, (free_function)fclose);
}

/*
void decrement_active_request_handlers(uword)
{
    pthread_mutex_lock(&_request_handlers.mutex);
        _request_handlers.active -=
}
*/

uword request_handler(uword thread_arg)
{
    uword word;
    FILE *file;
    size_t linecap = 0;
    ssize_t linelen;
    char *line = NULL;
    char *new_memory_allocation;
    int error;

    while (1) {
        pthread_mutex_lock(&_input_files_remaining_mutex);
            if (_input_files_remaining > 0) {
                _input_files_remaining -= 1;
            }
            else {
                pthread_mutex_unlock(&_input_files_remaining_mutex);

                pthread_mutex_lock(&_request_handlers.mutex);
                    _request_handlers.active -= 1;
                pthread_mutex_unlock(&_request_handlers.mutex);


                pthread_exit(0);
            }
        pthread_mutex_unlock(&_input_files_remaining_mutex);

        concurrent_queue_dequeue(&_input_file_queue, &word);

        file = word;
        while ((linelen = getline(&line, &linecap, file)) != -1) {
            new_memory_allocation = malloc(linelen);
            if (!new_memory_allocation) {
                perror("malloc");
                continue;
            }

            memcpy(new_memory_allocation, line, linelen);
            new_memory_allocation[linelen-1] = '\0';

            // This should be ok but might need to be replaced with try_enqueue
            concurrent_queue_enqueue(&_url_queue, new_memory_allocation);
            /* Perhaps useful for debugging purposes
            pthread_mutex_lock(&_stdout_mutex);
                printf("%.*s", linelen, line);
            pthread_mutex_unlock(&_stdout_mutex);
            */
        }
    }
}

uword url_handler(uword thread_arg)
{
    int should_exit = 0;
    char *url;
    int error;
    struct addrinfo *results = NULL;
    struct addrinfo *result = NULL;
    struct sockaddr_in *ipv4sock = NULL;
    struct in_addr *ipv4addr = NULL;
    char ipv4str[INET_ADDRSTRLEN];
    char ipstr[INET6_ADDRSTRLEN];

    while (1) {
        error = concurrent_queue_try_dequeue(&_url_queue, (uword*)&url);
        if (error) {
            pthread_mutex_lock(&_request_handlers.mutex);
                if (_request_handlers.active <= 0) {
                    should_exit = 1;
                }
            pthread_mutex_unlock(&_request_handlers.mutex);
            if (should_exit)
                pthread_exit(0);
            else
                usleep(100);
        }
        else {
            pthread_mutex_lock(&_getaddrinfo_mutex);
                error = getaddrinfo(url, NULL, NULL, &results);
            pthread_mutex_unlock(&_getaddrinfo_mutex);
            //if (error) {
                //fprintf(stderr, "%s", gai_strerror(error));
            //}
            for (result = results; result != NULL; result = result->ai_next) {
                if (result->ai_addr->sa_family == AF_INET) {
                    ipv4sock = (struct sockaddr_in*)(result->ai_addr);
                    ipv4addr = &(ipv4sock->sin_addr);
                    if (!inet_ntop(result->ai_family, ipv4addr,
                                   ipv4str, sizeof(ipv4str))) {
                        perror("Error converting ip to string");
                        continue;
                    }
                    else {
                        strncpy(ipstr, ipv4str, sizeof(ipstr));
                        ipstr[sizeof(ipstr) - 1] = '\0';
                        break;
                    }
                }
            }

            if (result == NULL) {
                strncpy(ipstr, "UNHANDLED", sizeof(ipstr));
                ipstr[sizeof(ipstr)-1] = '\0';
            }

            pthread_mutex_lock(&_stdout_mutex);
                if (url == 0)
                    printf("bad\n");
                printf("%s: %s\n", url, ipstr);
            pthread_mutex_unlock(&_stdout_mutex);

            freeaddrinfo(results);
            free(url);
        }
    }
}
