#ifndef peterdelevoryas_pthread_h
#define peterdelevoryas_pthread_h

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <pthread.h>
#include <errno.h>
#include <utility>

namespace peterdelevoryas {
  typedef uint64_t u64;

  class pthread {
    private:
      pthread_t m_id;

    public:
      // Constructor
      pthread() {}
      pthread(pthread_t id)
        : m_id(id)
      {
        // Do nothing
      }

      int init(void *(*start_routine)(void *), void *arg = nullptr) {
        int error;
        error = pthread_create(&m_id, nullptr, start_routine, arg);
        if (error) {
          errno = error;
          perror("pthread_create");
        }
        return error;
      }
      
      int join(void **value_ptr = nullptr) {
        int error;
        error = pthread_join(m_id, value_ptr);
        if (error) {
          errno = error;
          perror("pthread_join");
        }
        return error;
      }

      static void exit(void *value_ptr) {
        pthread_exit(std::forward<void*>(value_ptr));
      }

      ~pthread() {
        // Do nothing
      }
  };
}

#endif
