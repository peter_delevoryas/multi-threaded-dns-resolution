#include <pthread.h>
#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <semaphore.h>
#include <pthread.h>

//#include "concurrent_queue.h"

#define DEBUG

#define MAX_INPUT_FILES     10

/*
pthread_t *request_thread_pool;
pthread_t *resolver_thread_pool;
pthread_t *thread_pool;
int work_finished;
pthread_mutex_t work_finished_mutex;
pthread_mutex_t stdin_lock;
pthread_mutex_t thread_id_lock;
int thread_id = 0;
concurrent_queue cq;

void *request_thread_routine(void *ptr)
{
    pthread_mutex_lock(&thread_id_lock);
    char id = thread_id + '0';
    ++thread_id;
    pthread_mutex_unlock(&thread_id_lock);
    char *strptr;

    printf("thread_id: %c\n", id);

    for (char c = 'a'; c <= 'c'; ++c) {
        strptr = malloc(3);
        strptr[0] = id;
        strptr[1] = c;
        strptr[2] = 0;
        concurrent_queue_enqueue(&cq, strptr);

        pthread_mutex_lock(&stdin_lock);
        printf("+ %s\n", strptr);
        pthread_mutex_unlock(&stdin_lock);
    }

    pthread_exit(0);
}

void *resolver_thread_routine(void *ptr)
{
    char *str;
    while (1) {
        pthread_mutex_lock(&work_finished_mutex);
        if (work_finished <= 0) {
            pthread_mutex_unlock(&work_finished_mutex);
            pthread_exit(0);
        }
        work_finished -= 1;
        pthread_mutex_unlock(&work_finished_mutex);

        concurrent_queue_dequeue(&cq, &str);
        pthread_mutex_lock(&stdin_lock);
        printf("- %s\n", str);
        pthread_mutex_unlock(&stdin_lock);

        free(str);
    }
}

/*
int main(int argc, char *argv[])
{
    int THREAD_POOL_SIZE = atoi(argv[1]);
    thread_pool = malloc(2 * THREAD_POOL_SIZE * sizeof(pthread_t));

    int status;

    request_thread_pool = &thread_pool[0];
    resolver_thread_pool = &thread_pool[THREAD_POOL_SIZE];
    pthread_mutex_init(&work_finished_mutex, NULL);
    pthread_mutex_init(&stdin_lock, NULL);
    pthread_mutex_init(&thread_id_lock, NULL);

    work_finished = THREAD_POOL_SIZE * 3;

    concurrent_queue_init(&cq, 10);

    for (int i = 0; i < THREAD_POOL_SIZE; ++i) {
        pthread_create(&request_thread_pool[i], NULL, request_thread_routine, NULL);
        pthread_create(&resolver_thread_pool[i], NULL, resolver_thread_routine, NULL);
    }
    for (int i = 0; i < THREAD_POOL_SIZE; ++i) {
        pthread_join(request_thread_pool[i], NULL);
        pthread_join(resolver_thread_pool[i], NULL);
    }
    pthread_join(request_thread_pool[0], NULL);
    pthread_join(request_thread_pool[1], NULL);
    pthread_join(resolver_thread_pool[0], NULL);
    pthread_join(resolver_thread_pool[1], NULL);

    //concurrent_queue_free(&cq);
    free(cq.data);
}
*/

int main()
{
    printf("%zu, %zu\n", sizeof(pthread_mutex_t), sizeof(pthread_t));
}
