#include <pthread.h>
#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <semaphore.h>
#define DEBUG

#define MAX_INPUT_FILES     10

typedef size_t usize;

struct Queue {

};

pthread_t *request_thread_pool;
pthread_t *resolver_thread_pool;
pthread_mutex_t printf_mutex;
pthread_mutexattr_t mutexattr_errorcheck;

char **input_files;
usize n_input_files;
char *output_file;

bool filenames_are_unique(char **input_files, usize n_input_files, char *output_file) {
     // All filenames should be unique
    for (usize i = 0; i < n_input_files; ++i) {
        if (strcmp(input_files[i], output_file) == 0) {
            return false;
        }
        for (usize j = 0; j < n_input_files; ++j) {
            if (i == j)
                continue;
            if (strcmp(input_files[i], input_files[j]) == 0) {
                return false;
            }
        }
    }
    return true;
}

void *request_thread_routine(void *arg_ptr) {
    pthread_mutex_lock(&printf_mutex);
    printf("hello world\n");
    pthread_mutex_unlock(&printf_mutex);
    pthread_exit(NULL);
}

int main(int argc, char *argv[]) {
    long N_PROCESSORS_ONLINE;
    int status;

    // Check correct program usage
    if (argc < 3) {
        fprintf(stderr, "usage: ./program [input ...] [output]\n");
        return 1;
    } else {
        input_files = &argv[1];
        n_input_files = argc - 2;
        output_file = argv[argc - 1];
#ifdef DEBUG
        fprintf(stderr, "[DEBUG] input filenames receieved:\n");
        for (usize i = 0; i < n_input_files; ++i) {
            fprintf(stderr, "[DEBUG] input file %-4d  %30s\n", i, input_files[i]);
        }
        fprintf(stderr, "[DEBUG] output filename: %30s\n", output_file);
#endif
    }

    if (!filenames_are_unique(input_files, n_input_files, output_file)) {
        fprintf(stderr, "Duplicate filenames encountered: exiting...\n");
        return 1;
    }
#ifdef DEBUG
    fprintf(stderr, "[DEBUG] filenames are unique\n");
#endif

    status = pthread_mutexattr_init(&mutexattr_errorcheck);
    if (status < 0) {
        fprintf(stderr, "Error: pthread_mutexattr_init: %s\n", strerror(status));
        return 1;
    }
    status = pthread_mutexattr_settype(&mutexattr_errorcheck, PTHREAD_MUTEX_ERRORCHECK);
    if (status < 0) {
        fprintf(stderr, "Error: pthread_mutexattr_init: %s\n", strerror(status));
        return 1;
    }
    status = pthread_mutex_init(&printf_mutex, &mutexattr_errorcheck);
    if (status < 0) {
        fprintf(stderr, "Error: pthread_mutex_init: %s\n", strerror(status));
        return 1;
    }

    // Initialize number of processors
    N_PROCESSORS_ONLINE = sysconf(_SC_NPROCESSORS_ONLN);

    // Go ahead and malloc thread vector as single array, rather than
    // 2 separate pools.
    void *thread_buffer_ptr = malloc(sizeof(pthread_t) * N_PROCESSORS_ONLINE * 2);
    if (!thread_buffer_ptr) {
        fprintf(stderr, "Error allocating buffer of pthread_t\n");
        return 1;
    } else {
        request_thread_pool = thread_buffer_ptr;
        resolver_thread_pool = &request_thread_pool[N_PROCESSORS_ONLINE];
    }

    for (long i = 0; i < N_PROCESSORS_ONLINE; ++i) {
        status = pthread_create(&request_thread_pool[i], NULL, request_thread_routine, NULL);
        if (status < 0) {
            fprintf(stderr, "Error: pthread_create: thread %d: %d\n", i, status);
            return 1;
        }
    }
    for (long i = 0; i < N_PROCESSORS_ONLINE; ++i) {
        status = pthread_join(request_thread_pool[i], NULL);
        if (status < 0) {
            fprintf(stderr, "Error: pthread_join: thread %d: %d\n", i, status);
            continue;
        } else {
            printf("joined thread %d\n", i);
        }
    }
    
    free(request_thread_pool);

    status = pthread_mutexattr_destroy(&mutexattr_errorcheck);
    if (status < 0) {
        fprintf(stderr, "Error: pthread_mutexattr_destroy: %s\n", strerror(status));
        return 1;
    }
    status = pthread_mutex_destroy(&printf_mutex);
    if (status < 0) {
        fprintf(stderr, "Error: pthread_mutex_destroy: %s\n", strerror(status));
        return 1;
    }

    return 0;
}
