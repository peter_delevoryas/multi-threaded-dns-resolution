#ifndef ThreadPool_h
#define ThreadPool_h
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

typedef size_t  usize;
typedef ssize_t isize;
typedef void *  uword;

typedef struct
{
    pthread_mutex_t mutex;
    pthread_t *threads;
    usize count;
    isize active;
}
ThreadPool;

int ThreadPool_init(ThreadPool *thread_pool, usize count,
                    uword (*thread_routine)(uword));
int ThreadPool_join(ThreadPool *thread_pool);
void ThreadPool_free(ThreadPool *thread_pool);

#endif
