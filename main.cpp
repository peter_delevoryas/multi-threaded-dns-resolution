#include "peterdelevoryas_queue.h"
#include "peterdelevoryas_mutex.h"
#include "peterdelevoryas_pthread.h"

#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>

using peterdelevoryas::pthread;
using peterdelevoryas::mutex;
using peterdelevoryas::queue;
using peterdelevoryas::usize;
using peterdelevoryas::u64;

using std::vector;
using std::string;

constexpr auto URL_QUEUE_SIZE = 10;
constexpr auto USLEEP_TIME = 20;
mutex<queue<char*>> _url_queue(URL_QUEUE_SIZE);
mutex<queue<char*>> _file_queue(0);
mutex<usize> _active_request_threads(0);
mutex<int> _stdout_mutex(0);

void *request_routine(void *ptr) {
  int error;
  char *filename;
  FILE *file;
  char *line;
  usize linelen;
  usize linecap;
  char *str;

  while(1) {
    error = 0;
    _file_queue.lock();
      if (_file_queue.value().count() > 0) {
        _file_queue.value().pop(filename);
      } else {
        _file_queue.unlock();
        _active_request_threads.lock();
        _active_request_threads.value() -= 1;
        _active_request_threads.unlock();
        pthread_exit(0);
      }
    _file_queue.unlock();
    file = fopen(filename, "r");
    if (!file) {
      perror("fopen");
    } else {
      while ((linelen = getline(&line, &linecap, file)) != -1) {
        str = (char *) malloc(linelen);
        if (!str) {
          perror("malloc");
          continue;
        } else {
          memcpy(str, line, linelen);
          str[linelen - 1] = '\0';

          _url_queue.lock();
          error = _url_queue.value().push(str);
          _url_queue.unlock();
          while (error) {
            usleep(USLEEP_TIME);
            _url_queue.lock();
            error = _url_queue.value().push(str);
            _url_queue.unlock();
          }
        }
      }
    }
  }
}

void *resolver_routine(void *ptr) {
  int error;
  char *url;
  usize request_threadpool_size;

  while (1) {
    _url_queue.lock();
    error = _url_queue.value().pop(url);
    _url_queue.unlock();
    while (error) {
      _active_request_threads.lock();
      request_threadpool_size = _active_request_threads.value();
      _active_request_threads.unlock();
      if (request_threadpool_size == 0) {
        pthread_exit(0);
      } else {
        usleep(USLEEP_TIME);
        _url_queue.lock();
        error = _url_queue.value().pop(url);
        _url_queue.unlock();
      }
    }

    _stdout_mutex.lock();
    printf("%s\n", url);
    _stdout_mutex.unlock();
  }
}

int main(int argc, char *argv[]) {
  constexpr auto MIN_ARGS = 3;
  char *output_filename;
  vector<char*> input_filenames;
  usize n_threads;
  int status;

  if (argc < MIN_ARGS) {
    fprintf(stderr, "usage: %s [input ...] [output] [n_threads]\n", argv[0]);
    return -1;
  } else {
    output_filename = argv[argc - 2];
    _file_queue.value().resize(argc - 3);
    for(usize i = 0; i < argc - 3; ++i)
      _file_queue.value().push(argv[i + 1]);
    n_threads = strtoul(argv[argc - 1], NULL, 10);
  }

  vector<pthread> request_threadpool(n_threads % 2 ? n_threads / 2 + 1 : n_threads / 2);
  _active_request_threads.value() = request_threadpool.size();
  vector<pthread> resolver_threadpool(n_threads / 2);
  
  for(auto &thread: request_threadpool)
    thread.init(request_routine);
  for(auto &thread: resolver_threadpool)
    thread.init(resolver_routine);
  for(auto &thread: request_threadpool)
    thread.join();
  for(auto &thread: resolver_threadpool)
    thread.join();

}
