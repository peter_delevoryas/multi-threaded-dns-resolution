#ifndef peterdelevoryas_mutex_h
#define peterdelevoryas_mutex_h

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <errno.h>
#include <utility>

namespace peterdelevoryas {
  typedef uint64_t u64;
  typedef size_t usize;

  template<typename T>
  class mutex {
    private:
      pthread_mutex_t m_mutex;
      T m_value;

    public:
      // Constructor
      mutex(){}
      mutex(T value)
        : m_value(value)
      {
        int error;
        error = pthread_mutex_init(&m_mutex, nullptr);
        if (error) {
          errno = error;
          perror("pthread_mutex_init");
        }
      }

      mutex(const mutex<T> &m)
        : m_value(m.m_value)
      {
        int error;
        error = pthread_mutex_init(&m_mutex, nullptr);
        if (error) {
          errno = error;
          perror("pthread_mutex_init");
        }
      }

      T& value(void) {
        return m_value;
      }

      const T& value(void) const {
        return m_value;
      }

      int lock(void) {
        return pthread_mutex_lock(&m_mutex);
      }

      int unlock(void) {
        return pthread_mutex_unlock(&m_mutex);
      }

      int trylock(void) {
        return pthread_mutex_lock(&m_mutex);
      }

      ~mutex() {
        int error;
        error = pthread_mutex_destroy(&m_mutex);
        if (error) {
          errno = error;
          perror("pthread_mutex_destroy");
        }
      }
  };
}

#endif
