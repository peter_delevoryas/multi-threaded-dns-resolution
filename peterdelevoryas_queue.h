#ifndef peterdelevoryas_queue_h
#define peterdelevoryas_queue_h

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>
#include <pthread.h>
#include <iostream>
#include <string.h>

namespace peterdelevoryas {
  typedef uint64_t u64;
  typedef size_t usize;

  template<typename T>
  struct queue {
    private:
      usize m_head;
      usize m_tail;
      usize m_size;
      T *m_data;

    public:

      // Constructor
      queue(usize size = 0)
        : m_head(0), m_tail(0), m_size(size), m_data(nullptr)
      {
        m_data = (T*) malloc(sizeof(T) * size);
        if (!m_data) {
          perror("malloc");
        }
      }

      queue(const queue &q)
        : m_head(q.m_head), m_tail(q.m_tail), m_size(q.m_size), m_data(nullptr)
      {
        m_data = (T*) malloc(sizeof(T) * m_size);
        if (!m_data) {
          perror("malloc");
        }
        memcpy(m_data, q.m_data, m_size);
      }

      int pop(T &data) {
        assert(m_data);
        if (m_head == m_tail)
          return -1;
        data = m_data[m_head];
        m_head = (m_head + 1) % m_size;
        return 0;
      }

      int push(const T &data) {
        assert(m_data);
        auto idx = (m_tail + 1) % m_size;
        if (idx == m_head)
          return -1;
        m_data[m_tail] = data;
        m_tail = idx;
        return 0;
      }

      int resize(usize size) {
        assert(m_data);
        m_size = size;
        m_data = (T*) realloc(m_data, sizeof(T) * size);
        if (!m_data) {
          perror("realloc");
          return -1;
        }
        return 0;
      }

      usize size(void) {
        return m_size;
      }

      usize count(void) {
        usize tail = m_tail < m_head ? m_tail + m_size : m_tail;
        return tail - m_head;
      }

      // Destructor
      ~queue() {
        if (m_data) {
          free(m_data);
          m_data = nullptr;
        }
      }
  };
}

#endif
